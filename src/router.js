import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Questionnaire from './views/Questionnaire.vue'
import Results from './views/Results.vue'
import DataCheck from './views/DataCheck.vue'

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'login',
			component: Login
		},
		{
			path: '/questionnaire',
			name: 'questionnaire',
			component: Questionnaire
		},
		{
			path: '/results',
			name: 'results',
			component: Results
		},
		{
			path: '/datacheck',
			name: 'datacheck',
			component: DataCheck
		}
	]
})
