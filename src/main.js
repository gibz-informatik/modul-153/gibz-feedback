import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import firebase from 'firebase'

firebase.initializeApp({
	apiKey: "AIzaSyCcGR5Ji1sPJ3V97-zX9ylGMOxZluFr5zw",
	authDomain: "m153-gibz-feedback.firebaseapp.com",
	databaseURL: "https://m153-gibz-feedback.firebaseio.com",
	projectId: "m153-gibz-feedback",
	storageBucket: "m153-gibz-feedback.appspot.com",
	messagingSenderId: "808497757592",
	appId: "1:808497757592:web:fb21c9eb3cd57a16"
});

export const db = firebase.firestore();
export const auth = firebase.auth();

Vue.config.productionTip = false;

new Vue({
	router,
	render: h => h(App),
	created() {
		auth.onAuthStateChanged(this.authStateChange);
	},
	data: () => ({
		isAuthenticated: false,
		user: null,
		usersName: null
	}),
	mounted() {
		this.$on('toggleNavigationDrawer', this.toggleNavigationDrawer)
	},
	methods: {
		authStateChange(user) {
			this.user = user;
			const app = this;
			if (user) {
				this.isAuthenticated = true;
				db.collection('users').doc(user.uid).get().then(function(userDocument) {
					const userData = userDocument.data();
					app.usersName = `${userData['firstName']} ${userData['lastName']}`
				});
			} else {
				this.isAuthenticated = false;
				this.usersName = null;
			}
		},

		toggleNavigationDrawer() {
			console.log('toggleNavigationDrawer');
		}
	},
}).$mount('#app');