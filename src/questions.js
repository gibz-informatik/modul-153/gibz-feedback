export default {
	'social': [
		"Ich besuche den Unterricht am GIBZ gerne.",
		"Ich fühle mich wohl in dieser Klasse.",
		"Ich fühle mich von der Lehrperson verstanden und ernst genommen.",
		"Ich traue mich Fragen zu stellen.",
		"Vereinbarungen/Regeln werden in unserer Klasse eingehalten",
		"Die Lehrperson führt die Klasse mit Sicherheit und dem nötigen Überblick.",
	],
	'teaching': [
		"Die Lehrperson gibt die Lernziele bekannt.",
		"Die Lehrperson gibt Aufträge und Zeit für die Vertiefung des Stoffes.",
		"Die Lehrperson wirkt gut vorbereitet.",
		"Unterrichtssprache ist Standardsprache.",
		"Ich arbeite durchschnittlich die Hälfte der Unterrichtszeit eigenaktiv/eigenständig.",
		"Bei Einzelarbeiten kann ich ungestört lernen / arbeiten.",
		"Die Lehrperson zeigt mir Wege, wie ich lernen kann.",
		"Die Lehrperson gestaltet den Unterricht abwechslungsreich.",
		"Die Lehrperson erklärt den Unterrichtsstoff klar und verständlich.",
		"Hausaufgaben werden besprochen.",
	],
	'grading': [
		"Die Notengebung ist für mich nachvollziehbar.",
		"Die Notengebung empfinde ich in unserer Klasse/bei Ihnen korrekt.",
		"Die Lehrperson gibt die Prüfungen samt Fragen am nächsten Schultag zurück.",
		"Die bewerteten Aufgaben werden besprochen.",
		"Die Lehrperson hilft mir, meine Leistungen selber einzuschätzen."
	]
};